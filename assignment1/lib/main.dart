import 'package:flutter/material.dart';

void main() {
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  @override
  MyHomePageState createState() {
    return MyHomePageState();
  }
}

class MyHomePageState extends State<MyHomePage> {
  final formkey = GlobalKey<FormState>();
  // String name = "";
  String email = "";
  TextEditingController gmailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Sample App"),
      ),
      body: Column(
        children: [
          Form(
            key: formkey,
            child: Column(
              children: [
                Container(
                  width: 300,
                  child: TextFormField(
                    controller: gmailController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Email Must Not Be Empty";
                      }
                    },
                    decoration: InputDecoration(hintText: "Email"),
                  ),
                ),
                Container(
                    width: 300,
                    child: TextFormField(
                      controller: passwordController,
                      validator: (value) {
                        if (value == null || value.isEmpty)
                          return "Password Must Not Be Empty";
                      },
                      decoration: InputDecoration(hintText: "password"),
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (formkey.currentState!.validate()) {
                        print(gmailController.text);
                        print(passwordController.text);
                      }
                      ;
                    },
                    child: Text("Login"))
              ],
            ),
          )
        ],
      ),
    ));
  }
}
